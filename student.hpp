//
// Created by marsofandrew on 24.02.2020.
//

#ifndef LESSON_2_STUDENT_HPP
#define LESSON_2_STUDENT_HPP

class Student {
public:
    Student(int age);
    int getAge() const;

private:
    int age_;

};


#endif //LESSON_2_STUDENT_HPP
